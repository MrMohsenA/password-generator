from random import choices

from string import (
    ascii_uppercase,
    ascii_lowercase,
    digits,
    punctuation,
    ascii_letters
)

from utils import check_policy, storage_password


@storage_password
@check_policy
def password_generator(length=8, upper=False, lower=False, digit=False, punc=False):
    pool = ""

    if upper:
        pool = pool + ascii_uppercase

    if lower:
        pool = pool + ascii_lowercase

    if digit:
        pool = pool + digits

    if punc:
        pool = pool + punctuation

    if not pool:
        pool = pool + ascii_letters

    password = choices(pool, k=length)

    return "".join(password)
