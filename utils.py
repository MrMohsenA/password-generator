from functools import wraps

from arguments import added_argument
from password_policy import password_policy
from storage import storage_password_to_file


def check_policy(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        password = func(*args, **kwargs)
        status = password_policy(password)

        if status:
            msg = "The generated password is strong"
        else:
            msg = "The generated password is weak. It is not recommended to use a password"

        print(f"{msg}\nPassword is : {password}")

        return status, password

    return wrapper


def storage_password(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        status, password = func(*args, **kwargs)

        arguments = added_argument()
        file = arguments.file
        description = arguments.description

        if status and file:
            storage_password_to_file(
                password=password,
                filename=file,
                description=description
            )
            print(f"Password storage in {file}")

        return status, password

    return wrapper
