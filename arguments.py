from argparse import ArgumentParser


def added_argument():
    parser = ArgumentParser(
        description="Password Generator"
    )
    parser.add_argument(
        "--length",
        help="Choices length of password.",
        type=int,
        default=8,
    )
    parser.add_argument(
        "-l", "--lower",
        help="Use lower case ascii letters.",
        action="store_true"
    )
    parser.add_argument(
        "-u", "--upper",
        help="Use upper case ascii letters.",
        action="store_true"
    )
    parser.add_argument(
        "-d", "--digit",
        help="Use digit ascii letters.",
        action="store_true"
    )
    parser.add_argument(
        "-p", "--punc",
        help="Use punctuation ascii letters.",
        action="store_true"
    )

    parser.add_argument(
        "--file",
        help="Specify the password storage file(default:fixture/passwords)",
        type=str
    )

    parser.add_argument(
        "--description",
        help="Description about the password",
        type=str
    )

    args = parser.parse_args()

    return args
