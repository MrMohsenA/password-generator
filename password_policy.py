import re

from string import punctuation


def password_policy(password):
    check_length = len(password) >= 8
    check_lower = re.findall(r"[a-z]", password)
    check_upper = re.findall(r"[A-Z]", password)
    check_digit = re.findall(r"\d", password)
    check_punctuation = re.findall(rf"[{punctuation}]", password)

    checks = [check_length, check_lower, check_upper, check_digit, check_punctuation]
    return all(checks)
