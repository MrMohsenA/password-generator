from pathlib import Path

from config import BASE_DIR


def storage_password_to_file(password, filename="passwords", description=""):
    Path(f"{BASE_DIR}/fixtures").mkdir(parents=True, exist_ok=True)
    with open(file=f"fixtures/{filename}", mode="a", encoding="utf-8") as f:
        f.write(
            f"Password: {password} ==> Description: {description}\n\n"
        )
