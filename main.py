from arguments import added_argument
from password_generator import password_generator


def run():
    arguments = added_argument()

    length_of_password = arguments.length
    lower = arguments.lower
    upper = arguments.upper
    digit = arguments.digit
    punctuation = arguments.punc

    password_generator(
        length=length_of_password,
        lower=lower,
        upper=upper,
        digit=digit,
        punc=punctuation
    )


if __name__ == '__main__':
    run()
